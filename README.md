### This is bash script is use for init ssh (partch.anu.edu.au)
 Author Xuanming Gu
 
 Date March 12 ,2016
 
 Version 1.1

### if you want to config gitlab
 * copy content in gitlabconfig(in your Desktop)
 * go to gitlab.cecs.anu.edu.au/profile/keys
 * add key （paste）

### Code
```
#!/bin/bash
# This is bash script is use for init ssh (partch.anu.edu.au)
# Author Xuanming Gu
# Date March 12 ,2016
# Version 1.1

#if you want to config gitlab
# 1.copy content in gitlabconfig(in your Desktop)
# 2.go to gitlab.cecs.anu.edu.au/profile/keys
# 3.add key （paste）
read -p "Please input your UID? "  uniid
cd ~
cat .bash_profile
echo "alias sanu='ssh $uniid@partch.anu.edu.au'" >>.bash_profile
source .bash_profile
rm -f ~/.ssh/id_rsa;
rm -f ~/.ssh/id_rsa.pub;
ssh-keygen -t rsa -P "" -f ~/.ssh/id_rsa;
echo "geting old authorized_keys from server"
scp "$uniid@partch.anu.edu.au:~/.ssh/authorized_keys" ~/Desktop
cd ~/Desktop;
cat > gitlabconfig< ~/.ssh/id_rsa.pub
cat gitlabconfig >> authorized_keys
echo "Sending Public key to server!"
scp authorized_keys "$uniid@partch.anu.edu.au:~/.ssh"
rm authorized_keys
ssh "$uniid@partch.anu.edu.au"
exit 0


```
